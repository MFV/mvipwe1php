<?php 

/*
	Autor: 		Mats Vanselow
	Website: 	http://www.mats-vanselow.de
	
	Lizenz:		creative commons Namensnennung – Keine kommerzielle Nutzung – Weitergabe unter gleichen Bedingungen 3.0 Unported – http://creativecommons.org/licenses/by-nc-sa/3.0/deed.de
				creative commons Attribution-Noncommercial-Share Alike 3.0 Unported – http://creativecommons.org/licenses/by-nc-sa/3.0/
				Bei Verwendung und Weitergabe des Quellcodes muss ein Verweis auf den Autor und die Webseite erfolgen. In Begleitdokumentationen ist ebenfalls in angemessener Weise dieser Verweis anzubringen.
*/

require("mvIPWE1php.inc.php");

$ipwe1_sensor_id	= 2;							// ID des Sensors
$ipwe1_url			= "http://test.dyndns.org:866";	// Adresse zum IPWE1 Webinterface (mit Port)
$ipwe1_config_usr	= "wetteruser";					// Interface Login (optional)
$ipwe1_config_pw	= "wetterpw";					// Interface Login (optional)

// Sensordaten abfragen
$data_ipwe1_sensor = mvIPWE1php_get_history_data($ipwe1_sensor_id, $ipwe1_url, $ipwe1_config_usr, $ipwe1_config_pw);

// Prüfen auf korrekte Wetterermittling
if ($data_ipwe1_sensor == false) die("Wetterdaten konnten nicht ermittelt werden.");

// Auf spezifische Daten im Array zugreifen
echo "Die Temperatur um ".date("H:i:s", date("U") - $data_sel[Aktualisierung][0])." Uhr betraegt ".$data_ipwe1_sensor[Temperatur][0]." Grad Celsius.";

echo "\n\n";

// Array mit Rückgabewerten ausgeben
print_r($data_ipwe1_sensor);

?>