<?

/*
	Autor: 		Mats Vanselow
	Website: 	http://www.mats-vanselow.de
	
	Lizenz:		creative commons Namensnennung – Keine kommerzielle Nutzung – Weitergabe unter gleichen Bedingungen 3.0 Unported – http://creativecommons.org/licenses/by-nc-sa/3.0/deed.de
				creative commons Attribution-Noncommercial-Share Alike 3.0 Unported – http://creativecommons.org/licenses/by-nc-sa/3.0/
				Bei Verwendung und Weitergabe des Quellcodes muss ein Verweis auf den Autor und die Webseite erfolgen. In Begleitdokumentationen ist ebenfalls in angemessener Weise dieser Verweis anzubringen.
*/


function mvIPWE1php_get_history_data($sensorid, $ipwe1_url, $ipwe1_user = FALSE, $ipwe1_passwd = FALSE) {
	global $tmp_already_connected;
	
	
	$local_settings_wait_for_next_connect =		200000;			// Wenn bereits der IPWE1 aufgerufen wurde, x Mikrosekunden warten, damit dieser nicht überfordert ist
	$local_settings_wait_for_reconnect =		2000000;		// Bei Verbindungsabbruch: Wartezeit für Reconnect
	$local_settings_wait_reconnects =			2;				// Bei Verbindungsabbruch: Anzahl der Versuche
	
	$_values[Sensorid] = $sensorid;
	
	// Wenn bereits ein Connect ausgeführt wurde, kurz warten
	if ($tmp_already_connected)	usleep($local_settings_wait_for_next_connect);		
	$tmp_already_connected = true;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $ipwe1_url.'/history'.$sensorid.'.cgi');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
	// Header für Auth bauen
	if (!empty($ipwe1_user)) {
		curl_setopt($ch, CURLOPT_USERPWD, $ipwe1_user.":".$ipwe1_passwd);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	}

	// Verbinden (mehrere Versuche mit Pause)
	do {
		if ($i > 0) usleep($local_settings_wait_for_reconnect);
    	//$site = file_get_contents($ipwe1_url.'/history'.$sensorid.'.cgi',false, $context); 
    	$site = curl_exec($ch);
    	$i++;
    } while (($site == FALSE) AND ($i < $local_settings_wait_reconnects));
    
    curl_close($ch);
    
    // Keine Verbindung möglich, also abbrechen
    if ($site == FALSE) return FALSE;		

	// Anzahl der Verbindungsversuche ablegen
	$_values[ConnectCount] = $i;

	// Aktuelle Zeit (für Updatetime relevant)
	$_values[DateTime] = time();

    // Verarbeitung starten
    $lines=explode("\n", $site); 
    
    // Jede Zeile betrachten
    foreach($lines as $line) {
    	// Daten aufräumen
    	$line = str_replace('<tr>',"",$line);
    	$line = str_replace('</tr>',"",$line);
    	$line = str_replace('<td style="text-align: center;">',"",$line);
    	$line = str_replace('</td>',"",$line);
    	$line = trim($line);
		
		//echo $line;
		
		// Update ermitteln
		if (substr($line, -3, 3) == "sek") {
			$tmp_time = explode("min", $line);
			$_values[Aktualisierung][] = $tmp_time[0]*60 + trim(str_replace(substr($tmp_time[1], -3, 3), "", $tmp_time[1]));
		}
		
		
		// Bezeichnung des Sensors ermitteln
		if (substr($line, -8, 8) == "</title>") $_values[Bezeichnung] = trim(str_replace("<title>History","",str_replace(substr($line, -8, 8), "", $line)));
		
		// Werte ermitteln
		if (substr($line, -1, 1) == "C") $_values[Temperatur][] = trim(str_replace(substr($line, -2, 2), "", $line));
		if (substr($line, -1, 1) == "%") $_values[Luftfeuchtigkeit][] = trim(str_replace(substr($line, -1, 1), "", $line));
		if (substr($line, -4, 4) == "km/h") $_values[Windgeschwindigkeit][] = trim(str_replace(substr($line, -4, 4), "", $line));
		if (substr($line, -2, 2) == "mm") $_values[Regenmenge][] = trim(str_replace(substr($line, -2, 2), "", $line));
    }
    
    // Mittelwerte berechnen
    if (is_array($_values[Temperatur])) 			$_values[Temperatur][Mittelwert] = round((array_sum($_values[Temperatur]) / count($_values[Temperatur])),1);
    if (is_array($_values[Luftfeuchtigkeit])) 		$_values[Luftfeuchtigkeit][Mittelwert] = round((array_sum($_values[Luftfeuchtigkeit]) / count($_values[Luftfeuchtigkeit])),0);
    if (is_array($_values[Windgeschwindigkeit])) 	$_values[Windgeschwindigkeit][Mittelwert] = round((array_sum($_values[Windgeschwindigkeit]) / count($_values[Windgeschwindigkeit])),1);

	if (empty($_values[Aktualisierung])) return FALSE;		// Keine Daten vorhanden, also false zurückgeben
    return $_values;

    
}    

?>