# mvIPWE1php

### PHP Function zum Auslesen von Werten eines IPWE 1

Mit Hilfe dieser Function können die Historienseiten von Sensoren eines [IPWE 1][1] ausgelesen werden. Die Function liefert ein multidimensionales Array zurück. Die enthaltenen Wetterinformationen können beispielweise auf einer Webseite dargestellt werden.

[1]: http://www.amazon.de/gp/product/B0033WINP6?ie=UTF8&tag=mfv-21&linkCode=as2&camp=1638&creative=19454&creativeASIN=B0033WINP6



## Aufruf der Function

`$array = mvIPWE1php_get_history_data($ipwe1_sensor_id, $ipwe1_url, $ipwe1_user = FALSE, $ipwe1_passwd = FALSE);`

Der Function `mvIPWE1php_get_history_data`  sind 

* die Sensorid (`$ipwe1_sensor_id`)
* die URL zum IPWE 1 Webinterface (`$ipwe1_url`)
* optional der Benutzer für den Webinterface-Login (`$ipwe1_user`)
* sowie optional das Passwort für den Webinterface-Login (`$ipwe1_passwd`)

beim Aufruf mitzugeben.



## Rückgabe der Function

Die Function liefert bei erfolgreicher Abfrage ein multidimensionales Array mit Daten zurück. Sollte die Abfrage fehlschlagen, wird false zurückgeliefert. Dies sollte im Rahmen der Weiterverarbeitung entsprechend überprüft werden.

### Aufbau des multidemsionalen Arrays

* Sensorid: Abgefragte Sensor ID
* ConntectCount: Anzahl der Verbindungsversuche mit dem IPWE 1
* DateTime: Timestamp der Abfrage
* Bezeichnung: Technische Bezeichnung des Sensors
* Aktualisierung
	* 0 ... 4: Alter der Werte in Sekunden
	* Mittelwert: Berechneter Mittelwert über alle fünf Messungen
* Temperatur
	* 0 ... 4: Temperatur in Grad Celsius zum jeweiligen Zeitpunkt
	* Mittelwert: Berechneter Mittelwert über alle fünf Messungen
* Luftfeuchtigkeit
	* 0 ... 4: Luftfeuchtigkeit in Prozent zum jeweiligen Zeitpunkt
	* Mittelwert: Berechneter Mittelwert über alle fünf Messungen
* Windgeschwindigkeit
	* 0 ... 4: Windgeschwindigkeit in km/h zum jeweiligen Zeitpunkt
	* Mittelwert: Berechneter Mittelwert über alle fünf Messungen
* Regenmenge
	* 0 ... 4: Regenmenge der letzten Stunde in mm zum jeweiligen Zeitpunkt


### Beispiel einer Rückgabe der Function

Rückgabe der Function bei Abfrage eines  Funk-Temperatur-/Luftfeuchteaußensensoren S 300 TH:

	Array
	(
	    [Sensorid] => 2
	    [ConnectCount] => 1
	    [DateTime] => 1321134427
	    [Bezeichnung] => T/F-Sensor 2
	    [Aktualisierung] => Array
	        (
	            [0] => 337
	            [1] => 513
	            [2] => 689
	            [3] => 866
	            [4] => 1042
	    	)

	    [Temperatur] => Array
	        (
	            [0] => 23.4
	            [1] => 23.5
	            [2] => 23.5
	            [3] => 23.6
	            [4] => 23.6
	            [Mittelwert] => 23.5
 	        )
	
 	   [Luftfeuchtigkeit] => Array
	        (
	            [0] => 45
	            [1] => 45
	            [2] => 45
	            [3] => 45
	            [4] => 45
	            [Mittelwert] => 45
 	        )

	)



## Installation

Installationsvoraussetzung ist PHP5. Zudem ist es notwendig vom Webserver das Webinterface des IPWE 1 erreichen zu können. Ggf. ist eine Bekanntgabe der dynamischen IP mit einem Service wie DynDns sowie eine Firewall Freigabe notwendig.

Die PHP-Dateien mvIPWE1php.inc.php sowie demo.php auf den Server in ein Verzeichnis kopieren. In demo.php die hinterlegten Daten zur SensorID, URL sowie Benutzername und Passwort anpassen. Im Browser demo.php aufrufen.



## Lizenz

Autor: Mats Vanselow - [http://www.mats-vanselow.de][2]
	
creative commons Namensnennung - Keine kommerzielle Nutzung - Weitergabe unter gleichen Bedingungen 3.0 Unported  - [http://creativecommons.org/licenses/by-nc-sa/3.0/deed.de][3]

creative commons Attribution-Noncommercial-Share Alike 3.0 Unported - [http://creativecommons.org/licenses/by-nc-sa/3.0/][4]

[2]: http://www.mats-vanselow.de
[3]: http://creativecommons.org/licenses/by-nc-sa/3.0/deed.de
[4]: http://creativecommons.org/licenses/by-nc-sa/3.0/

Bei Verwendung und Weitergabe des Quellcodes muss ein Verweis auf den Autor und die Webseite erfolgen. In Begleitdokumentationen ist ebenfalls in angemessener Weise dieser Verweis anzubringen.  
